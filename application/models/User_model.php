<?php
class User_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

	public function get_users($id = FALSE)
	{
		if ($id === FALSE)
		{
		        $query = $this->db->get('user');
		        return $query->result_array();
		}

		$query = $this->db->get_where('user', array('id' => $id));
		return $query->row_array();
	}

	public function create_user()
	{

	    $password = url_title($this->input->post('title'), 'dash', TRUE);
	    $data = array(
		'username' => $this->input->post('username'),
		'email' => $this->input->post('email'),
		//'password' => $password,
		//'enabled' => True
	    );

	    return $this->db->insert('user', $data);
	}


	public function edit_user($id)
	{

	    $password = url_title($this->input->post('title'), 'dash', TRUE);
	    $data = array(
		'username' => $this->input->post('title'),
		'password' => $password,
		'email' => $this->input->post('title')
	    );
	    $this->db->where('id', $id);
	    return $this->db->replace('user', $data);
	}


	public function delete_user($id)
	{
	    $data = array(
		'enabled' => False
	    );
	    $this->db->where('id', $id);
	    return $this->db->replace('user', $data);
	}

}
