<?php
class Users extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('user_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['users'] = $this->user_model->get_users();
		$data['title'] = 'Users List';

		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
        }

	public function view($id = NULL)
	{
		$data['user_item'] = $this->user_model->get_users($id);

		if (empty($data['user_item']))
		{
		        show_404();
		}

		$data['title'] = $data['user_item']['username'];

		$this->load->view('templates/header', $data);
		$this->load->view('users/view', $data);
		$this->load->view('templates/footer');
	}

	public function create()
	{
	    $this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a new User';

	    $this->form_validation->set_rules('username', 'Username', 'required');
	    $this->form_validation->set_rules('email', 'E-mail', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
		$this->load->view('templates/header', $data);
		$this->load->view('users/create');
		$this->load->view('templates/footer');

	    }
	    else
	    {
		$this->user_model->create_user();
                $data['users'] = $this->user_model->get_users();
		$data['title'] = 'Users List';

		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	    }
	}

	public function edit()
	{
	    $this->load->helper('form');
	    $this->load->library('form_validation');

	    $data['title'] = 'Create a news item';

	    $this->form_validation->set_rules('username', 'Username', 'required');
	    $this->form_validation->set_rules('email', 'E-mail', 'required');

	    if ($this->form_validation->run() === FALSE)
	    {
		$this->load->view('templates/header', $data);
		$this->load->view('user/create');
		$this->load->view('templates/footer');

	    }
	    else
	    {
		$this->news_model->edit_user();
		$this->load->view('users/success');
	    }
	}

	public function delete()
	{
	    $data['title'] = 'Delete User';

	    if ($this->form_validation->run() === FALSE)
	    {
		$this->load->view('templates/header', $data);
		$this->load->view('users/create');
		$this->load->view('templates/footer');

	    }
	    else
	    {
		$this->news_model->delete_user();
		$this->load->view('users/success');
	    }
	}
}
